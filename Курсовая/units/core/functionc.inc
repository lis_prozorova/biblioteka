<?php

	function GetValue($get_name, $post_name, $default_value = null) {
		if ($get_name && isset($_GET[$get_name])) return $_GET[$get_name];
		if ($post_name && isset($_POST[$post_name])) return $_POST[$post_name];
		return $default_value;
	}

	function debug(& $var) {
		if (DEBUG == true) { echo '<pre>', var_export($var), '</pre>'; }
	}
