<?php

class TAdmin extends IBaseUnit {

    public function __construct() {
        parent::__construct(1, __DIR__.UNIT_TPLS, 'admin');

        TController::RegisterScene('prepare', 'idle', 'null', $this, 'DoAuthTest');
        TController::RegisterScene('view', 'main', 'null', $this, 'OnOutMainInfo');
    }

    public function DoAuthTest() {
        if (!isset($_SESSION['auth_user'])) {
            $login = trim((string)GetValue('login', 'login', ''));
            $password = trim((string)GetValue('password', 'password', ''));

            if ($login && $password) {
                $login = addslashes($login);
                $passToBD = addslashes($this->EncryptionAlgorithm($password));

                $result = $this->db->query("SELECT * FROM `admin` WHERE `login`='{$login}' AND `password`='{$passToBD}' LIMIT 1");
                $res=mysqli_fetch_assoc($result);
                if ($result && $res) {
                    $_SESSION['auth_user'] = $res;
                    return;
                }
            }

            TViewer::LoadConfig('auth');
            TViewer::Browse('auth.tpl');
            die();
        }
    }

    public function OnOutMainInfo() { }
    public function DoLeave() { if (isset($_SESSION['auth_user'])) unset($_SESSION['auth_user']); }
    public function GetData($id) { }


    private function EncryptionAlgorithm($str) {
        $salt = "Mj7^@1T*s}a2`qQ";
        $md5 = md5($str);
        $pass = [];
        for($i = 0; $i < 16; $i++) {
            $pass[] = $md5[$i * 2];
            $pass[] = $md5[$i * 2 + 1 ];
            $pass[] = $salt[$i];
        }

        return md5(implode('',$pass));
    }
}

new TAdmin();
