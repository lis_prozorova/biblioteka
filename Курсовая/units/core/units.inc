<?php

	class IBaseUnit {
		protected $dir;
		protected $dir_rel;
        /** @var mysqli */ protected $db;
		protected $SI = 0;

		public function __construct($SI, $dir, $unit_name = null) {
			$this->SI = $SI;
			$this->dir = $dir;

			$this->dir_rel = str_replace(ROOT_DIR, '', $dir);
			$this->db = iMySQLDataBase::$db;
		}

		public function GetSI() { return $this->SI; }
	}
