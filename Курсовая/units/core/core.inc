<?php

mb_internal_encoding("UTF-8");

require ROOT_DIR . 'core/functions.inc';      // Просто глобальные функции
require ROOT_DIR . 'core/controller.inc';     // Контроллер, вызов функций юнитов от запросов
require ROOT_DIR . 'core/viewer.inc';         // Отображение, монипуляция с буфером вывода, темплейтами...
require ROOT_DIR . 'core/mysql.inc';          // Конкретная БД MySQL
require ROOT_DIR . 'core/units.inc';          // Основа под юниты (модули)
