<?php

class TController {
	static protected $Scenes;
	static protected $ActiveScenes;
	static protected $ActiveLayers;
	
	public function __construct() { }
	
	static public function RegisterScene($stage, $scene, $layer, $Unit, $function) {
		if (!method_exists($Unit, $function)) return false;
		if (!isset(static::$Scenes[$stage][$scene][$layer])) { static::$Scenes[$stage][$scene][$layer] = []; }
		static::$Scenes[$stage][$scene][$layer][] = ['Unit' => $Unit, 'function' => $function];
	}

    static public function ExecuteScene($stage, $scene, $layer) {
		if (isset(static::$Scenes[$stage][$scene][$layer])) {
			foreach (static::$Scenes[$stage][$scene][$layer] as $action) {
				$function = $action['function'];
				$Unit = $action['Unit'];
				$Unit->$function();
			}
		}
	}

        // Анализ входящего запроса
	static public function Prepare() {
		static::$ActiveScenes = GetValue('s', 's', [0 => 'main']);
		static::$ActiveLayers = GetValue('l', 'l', [0 => 'null']);

		if (!is_array(static::$ActiveScenes)) static::$ActiveScenes = [static::$ActiveScenes];
		if (!is_array(static::$ActiveLayers)) static::$ActiveLayers = [static::$ActiveLayers];

		foreach (static::$ActiveScenes as $key => & $value) {
            $value = (string)$value;
			if (trim($value)=='') $value = 'main';
			if (!isset(static::$ActiveLayers[$key])) static::$ActiveLayers[$key] = 'null';
		}
	}

        //	0 - init, 1 - prepare, 2 - work, 3 - view
	static public function Execute() {
		TController::ExecuteScene('init', 'idle', 'null');
		foreach (static::$ActiveScenes as $key => $scene) {
			TController::ExecuteScene('init', $scene, 'idle');
			TController::ExecuteScene('init', $scene, static::$ActiveLayers[$key]);
		}

		TController::ExecuteScene('prepare', 'idle', 'null');
		foreach (static::$ActiveScenes as $key => $scene) {
			TController::ExecuteScene('prepare', $scene, 'idle');
			TController::ExecuteScene('prepare', $scene, static::$ActiveLayers[$key]);
		}

		TController::ExecuteScene('work', 'idle', 'null');
		foreach (static::$ActiveScenes as $key => $scene) {
			TController::ExecuteScene('work', $scene, 'idle');
			TController::ExecuteScene('work', $scene, static::$ActiveLayers[$key]);
		}

		TController::ExecuteScene('view', 'idle', 'null');
		foreach (static::$ActiveScenes as $key => $scene) {
			TController::ExecuteScene('view', $scene, 'idle');
			TController::ExecuteScene('view', $scene, static::$ActiveLayers[$key]);
		}
	}

}
