<?php

class TViewer {
    static protected $TemplateFolder;

    static protected $Section = [];
    static protected $Styles = [];
    static protected $Scripts = [];
    static protected $ExecScripts = [];

    static public $Config = null;
    static public $RelFolder;

    static public function Initialization($TemplateFolder) {
        static::$TemplateFolder = $TemplateFolder;
        static::$RelFolder = '/' . str_replace(ROOT_DIR, '', static::$TemplateFolder);

        static::$Config['name'] = '';
        static::$Config['tpl'] = 'index.tpl';

        static::LoadConfig('default');
    }

    static public function LoadConfig($config='') {
        if (static::$Config['name'] == (string)$config) return true;

        static::$Section = [];
        static::$Styles = [];
        static::$Scripts = [];
        static::$ExecScripts = [];

        $conf = static::$TemplateFolder.'cfg/'.$config.'.inc';
        if ($config && file_exists($conf)) include $conf;
    }

    static public function BrowseToVar($file, $params = null) {
        ob_start();
        if (file_exists($file)) include $file;
        else echo 'File "', $file, '" not exists';
        $var = ob_get_contents();
        ob_end_clean();

        return $var;
    }

    static public function Browse($file, $params = null) {
        if (file_exists($file)) include $file;
        elseif (file_exists(static::$TemplateFolder.$file)) include static::$TemplateFolder.$file;
        else echo 'File "', $file, '" not exists';
    }

    static public function BrowseSection($section_name) {
        if (isset(static::$Section[$section_name])) {
            foreach (static::$Section[$section_name] as $out_template)
                static::BrowseWP($out_template['file'], $out_template['params']);
        }
    }

    static public function AddStyleSheet($section, $name, $file, $media=null) {
        static::$Styles[$section][$name] = ['file' => $file, 'media' => $media];
    }

    static public function OutStyles($section) {
        if (isset(static::$Styles[$section])) {
            foreach (static::$Styles[$section] as $style) {
                echo '<link rel="stylesheet" href="', $style['file'], '"';
                if ($style['media']) echo ' media="', $style['media'], '"';
                echo ' type="text/css">';
            }
        }
    }

    static public function OutScripts($section) {
        if (isset(static::$Scripts[$section])) {
            foreach (static::$Scripts[$section] as $out_file)
                echo '<script type="text/javascript" src="', $out_file, '"></script>';
        }
    }
}
