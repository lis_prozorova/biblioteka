<?php

iMySQLDataBase::create(DB_SERV, DB_USER, DB_PASS, DB_NAME);
if (!iMySQLDataBase::$db) die('Нет соединения с Базой данных!');

TViewer::Initialization(TPL_DIR);

require 'units/admin/admin.inc';                    //	SI : 1

session_start();

TController::Prepare();
TController::Execute();
