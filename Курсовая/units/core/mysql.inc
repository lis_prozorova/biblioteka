<?php

abstract class iMySQLDataBase {
    /** @var mysqli */ static public $db = NULL;
    static public function create($serv, $user, $pass, $db_name) {
        self::$db = mysqli_connect($serv, $user, $pass, $db_name);
    }
}
