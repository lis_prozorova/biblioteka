<!DOCTYPE html>
<html>
  <head>
   	<title>Книжечка</title>
   	<meta charset="utf-8" /><?php

        TViewer::OutStyles('head');
        TViewer::OutScripts('head');

    ?></head>
    <body>
        <div id="modal"></div>
        <div id="container-top"></div>
        <div id="container-left"><?php TViewer::BrowseSection('left'); ?></div>
        <div id="container-center"></div>
    </body>
</html>
