<?php

define('RUN_TYPE', 'ADMIN');
define('ROOT_DIR', __DIR__ . '/');
define('CORE', ROOT_DIR.'core/');
define('TPL_DIR', ROOT_DIR.'templates/adm/');
define('UNIT_TPLS', '/adm/');

define('DEBUG', true);                          // проверка для вывода доп инфы, нужно при создании

require ROOT_DIR . 'core/core.inc';             // сборка ядра
require ROOT_DIR . 'config/adm/params.inc';     // параметры (БД покаместь)
require ROOT_DIR . 'config/adm/prepare.inc';    // сборка логики работы системы

TViewer::Browse(TViewer::$Config['tpl']);       // Если нигде не умерло, то собираем страничку
